mod game;
use game::{Board, Rule};
mod ui;
use ui::Analyzer;

fn main() {
    eframe::run_native(
        "Cryptid analyzer",
        eframe::NativeOptions::default(),
        Box::new(|_| Box::new(Analyzer::new(Board::empty(5), Rule::normal_set()))),
    );
}
