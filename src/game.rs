use itertools::Itertools;
use serde::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    fmt::{self, Display, Formatter},
};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct Hex {
    x: i32,
    y: i32,
}

impl Hex {
    pub fn x(&self) -> i32 {
        self.x
    }
    pub fn y(&self) -> i32 {
        self.y
    }
    pub fn z(&self) -> i32 {
        -(self.x + self.y)
    }

    pub fn dist(self, other: Hex) -> i32 {
        ((self.x - other.x).abs() + (self.y - other.y).abs() + (self.z() - other.z()).abs()) / 2
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Terrain {
    Forest,
    Water,
    Swamp,
    Mountain,
    Desert,
}

impl Display for Terrain {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct Building {
    pub ty: BuildingType,
    pub color: BuildingColor,
}

impl Display for Building {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{} {}", self.color, self.ty)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum BuildingType {
    Shack,
    Stone,
}

impl Display for BuildingType {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum BuildingColor {
    White,
    Blue,
    Green,
}

impl Display for BuildingColor {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Animal {
    Bear,
    Cougar,
}

impl Display for Animal {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct Tile {
    pub terrain: Option<Terrain>,
    pub animal: Option<Animal>,
    pub building: Option<Building>,
    pub player_info: Vec<Option<bool>>,
}

impl Tile {
    fn has_property(&self, property: Property) -> bool {
        match property {
            Property::Terrain(terrain) => self.terrain.map_or(false, |t| t == terrain),
            Property::Animal(animal) => self.animal.map_or(false, |a| a == animal),
            Property::BuildingType(ty) => self.building.map_or(false, |b| b.ty == ty),
            Property::BuildingColor(color) => self.building.map_or(false, |b| b.color == color),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Property {
    Terrain(Terrain),
    Animal(Animal),
    BuildingType(BuildingType),
    BuildingColor(BuildingColor),
}

impl Display for Property {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Property::Terrain(terrain) => write!(f, "{}", terrain),
            Property::Animal(animal) => write!(f, "{}", animal),
            Property::BuildingType(ty) => write!(f, "{}", ty),
            Property::BuildingColor(color) => write!(f, "{}", color),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Rule {
    Direct { property: Property, max_dist: i32 },
    Or { left: Box<Rule>, right: Box<Rule> },
    Not { rule: Box<Rule> },
}

impl Rule {
    fn matches_fast(
        &self,
        properties_rev: &HashMap<Property, usize>,
        property_dist_map: &[i32],
    ) -> bool {
        match self {
            Rule::Direct { property, max_dist } => {
                property_dist_map[properties_rev[&property]] <= *max_dist
            }
            Rule::Or { left, right } => {
                left.matches_fast(properties_rev, property_dist_map)
                    || right.matches_fast(properties_rev, property_dist_map)
            }
            Rule::Not { rule } => !rule.matches_fast(properties_rev, property_dist_map),
        }
    }

    pub fn normal_set() -> Vec<Rule> {
        let mut set = Vec::new();
        // TODO Use strum here
        let terrains = [
            Terrain::Forest,
            Terrain::Water,
            Terrain::Swamp,
            Terrain::Mountain,
            Terrain::Desert,
        ];

        for i in 0..terrains.len() {
            for j in i + 1..terrains.len() {
                set.push(Rule::Or {
                    left: Box::new(Rule::Direct {
                        property: Property::Terrain(terrains[i]),
                        max_dist: 0,
                    }),
                    right: Box::new(Rule::Direct {
                        property: Property::Terrain(terrains[j]),
                        max_dist: 0,
                    }),
                });
            }
        }

        for t in terrains {
            set.push(Rule::Direct {
                property: Property::Terrain(t),
                max_dist: 1,
            });
        }

        set.push(Rule::Or {
            left: Box::new(Rule::Direct {
                property: Property::Animal(Animal::Cougar),
                max_dist: 1,
            }),
            right: Box::new(Rule::Direct {
                property: Property::Animal(Animal::Bear),
                max_dist: 1,
            }),
        });

        set.push(Rule::Direct {
            property: Property::Animal(Animal::Cougar),
            max_dist: 2,
        });
        set.push(Rule::Direct {
            property: Property::Animal(Animal::Bear),
            max_dist: 2,
        });

        set.push(Rule::Direct {
            property: Property::BuildingType(BuildingType::Stone),
            max_dist: 2,
        });
        set.push(Rule::Direct {
            property: Property::BuildingType(BuildingType::Shack),
            max_dist: 2,
        });

        set.push(Rule::Direct {
            property: Property::BuildingColor(BuildingColor::White),
            max_dist: 3,
        });
        set.push(Rule::Direct {
            property: Property::BuildingColor(BuildingColor::Blue),
            max_dist: 3,
        });
        set.push(Rule::Direct {
            property: Property::BuildingColor(BuildingColor::Green),
            max_dist: 3,
        });

        set
    }
}

impl Display for Rule {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Rule::Direct { property, max_dist } => write!(f, "{} within {}", property, max_dist),
            Rule::Or { left, right } => write!(f, "{} or {}", left, right),
            Rule::Not { rule } => write!(f, "not {}", rule),
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Board {
    players: u8,
    hexes: Vec<Hex>,
    tiles: Vec<Tile>,
}

impl Board {
    pub fn empty(players: u8) -> Self {
        let mut board = Board {
            players,
            hexes: Vec::new(),
            tiles: Vec::new(),
        };
        for i in 0..12 {
            for j in 0..9 {
                board.hexes.push(Hex {
                    x: j as i32 - (i / 2) as i32,
                    y: i as i32,
                });
                board.tiles.push(Tile {
                    terrain: None,
                    animal: None,
                    building: None,
                    player_info: vec![None; players as usize],
                });
            }
        }
        board
    }

    pub fn iter(&self) -> impl Iterator<Item = (Hex, &Tile)> + '_ {
        Iterator::zip(self.hexes.iter().copied(), self.tiles.iter())
    }
    pub fn iter_mut(&mut self) -> impl Iterator<Item = (Hex, &mut Tile)> + '_ {
        Iterator::zip(self.hexes.iter().copied(), self.tiles.iter_mut())
    }

    pub fn players(&self) -> usize {
        self.players.into()
    }

    pub fn calculate(&self, rules: Vec<Rule>) -> Stats {
        let mut hexes_rev = HashMap::with_capacity(self.hexes.len());
        for (i, hex) in self.hexes.iter().enumerate() {
            hexes_rev.insert(*hex, i);
        }

        let mut properties = Vec::new();
        {
            let mut base_rules: Vec<&Rule> = Vec::with_capacity(rules.len());
            for rule in rules.iter() {
                base_rules.push(rule);
            }
            while let Some(rule) = base_rules.pop() {
                match rule {
                    Rule::Direct { property, .. } => {
                        properties.push(*property);
                    }
                    Rule::Or { left, right } => {
                        base_rules.push(left);
                        base_rules.push(right);
                    }
                    Rule::Not { rule } => base_rules.push(rule),
                }
            }
        }

        let mut properties_rev = HashMap::with_capacity(properties.len());
        for (i, prop) in properties.iter().enumerate() {
            properties_rev.insert(*prop, i);
        }

        let mut property_dist_map = Vec::with_capacity(self.hexes.len());
        for hex in self.hexes.iter() {
            let mut dist_map = Vec::with_capacity(properties.len());
            for &prop in properties.iter() {
                let min = self
                    .iter()
                    .filter_map(|(hex2, tile)| {
                        let dist = hex.dist(hex2);
                        if tile.has_property(prop) {
                            Some(dist)
                        } else {
                            None
                        }
                    })
                    .min()
                    .unwrap_or(i32::MAX);
                dist_map.push(min);
            }
            property_dist_map.push(dist_map);
        }

        let mut rule_map = Vec::with_capacity(self.hexes.len());
        for map in property_dist_map.iter() {
            let mut rule_values = Vec::with_capacity(rules.len());
            for rule in rules.iter() {
                rule_values.push(rule.matches_fast(&properties_rev, map));
            }
            rule_map.push(rule_values);
        }

        let mut unique_rulesets = Vec::new();
        let mut possible_solutions = vec![None; self.hexes.len()];
        'rulesets: for ruleset in (0..rules.len()).combinations(self.players()) {
            let mut possible_hexes = Vec::new();
            for (idx, (rule_values, tile)) in
                Iterator::zip(rule_map.iter(), self.tiles.iter()).enumerate()
            {
                if !ruleset.iter().enumerate().all(|(player, &rule)| {
                    tile.player_info[player].map_or(true, |val| val == rule_values[rule])
                }) {
                    continue 'rulesets;
                }

                if ruleset.iter().all(|&rule| rule_values[rule]) {
                    possible_hexes.push(idx);
                }
            }

            if let [hex_idx] = possible_hexes[..] {
                let owned_ruleset: Ruleset = ruleset.iter().map(|x| (*x).clone()).collect();
                unique_rulesets.push((owned_ruleset, hex_idx));
                possible_solutions[hex_idx] = Some(unique_rulesets.len() - 1);
            }
        }

        Stats {
            hexes: self.hexes.clone(),
            hexes_rev,
            properties,
            properties_rev,
            property_dist_map,
            rules,
            rule_map,
            unique_rulesets,
            possible_solutions,
        }
    }
}

type PerHex<T> = Vec<T>;
type Ruleset = Vec<usize>;

#[derive(Debug, Clone)]
pub struct Stats {
    hexes: Vec<Hex>,
    hexes_rev: HashMap<Hex, usize>,

    properties: Vec<Property>,
    properties_rev: HashMap<Property, usize>,
    property_dist_map: PerHex<Vec<i32>>,

    rules: Vec<Rule>,
    rule_map: PerHex<Vec<bool>>,

    unique_rulesets: Vec<(Ruleset, usize)>,
    possible_solutions: PerHex<Option<usize>>,
}

impl Stats {
    pub fn solution_for(&self, hex: Hex) -> Option<Vec<Rule>> {
        self.possible_solutions[self.hexes_rev[&hex]].map(|idx| {
            self.unique_rulesets[idx]
                .0
                .iter()
                .map(|rule| self.rules[*rule].clone())
                .collect()
        })
    }

    pub fn matching_rules_for(&self, hex: Hex) -> Vec<Rule> {
        self.rule_map[self.hexes_rev[&hex]]
            .iter()
            .enumerate()
            .filter_map(|(rule_id, &works)| {
                if works {
                    Some(self.rules[rule_id].clone())
                } else {
                    None
                }
            })
            .collect()
    }
}
