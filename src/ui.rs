use crate::game::{
    Animal, Board, Building, BuildingColor, BuildingType, Hex, Rule, Stats, Terrain, Tile,
};
use egui::{
    text::LayoutJob, Align, CentralPanel, Color32, Context, FontId, PointerButton, Pos2, Rect,
    Rounding, Sense, Shape, Stroke, TextFormat, Vec2, Widget,
};
use std::{f32::consts::PI, iter};

#[derive(Debug)]
pub struct Analyzer {
    rules: Vec<Rule>,
    board: Board,

    stats: Option<Stats>,
    selected: Selected,
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct Selected {
    terrain: Option<Terrain>,
    animal: Option<Animal>,
    building: Option<Building>,
    player: Option<usize>,
}

impl Analyzer {
    pub fn new(board: Board, rules: Vec<Rule>) -> Self {
        Analyzer {
            selected: Selected {
                terrain: None,
                animal: None,
                building: None,
                player: None,
            },
            stats: None,

            board,
            rules,
        }
    }

    fn recalculate(&mut self) {
        self.stats = Some(self.board.calculate(self.rules.clone()));
    }
}

impl eframe::App for Analyzer {
    fn update(&mut self, ctx: &Context, _frame: &mut eframe::Frame) {
        CentralPanel::default().show(ctx, |ui| {
            ui.horizontal(|ui| {
                if ui.button("Save").clicked() {
                    let data = bincode::serialize(&self.board).expect("failed to serialize board");
                    std::fs::write("board.bin", &data).expect("failed to write to file");
                }

                if ui.button("Load").clicked() {
                    let data = std::fs::read("board.bin").expect("failed to read from file");
                    self.board = bincode::deserialize(&data).expect("failed to deserialize board");
                    self.recalculate();
                }

                if ui.button("Calculate").clicked() {
                    self.recalculate();
                }
            });
            ui.separator();

            ui.vertical(|ui| {
                ui.horizontal(|ui| {
                    for terrain in [
                        Terrain::Forest,
                        Terrain::Water,
                        Terrain::Swamp,
                        Terrain::Mountain,
                        Terrain::Desert,
                    ] {
                        ui.radio_value(
                            &mut self.selected.terrain,
                            Some(terrain),
                            terrain.to_string(),
                        );
                    }
                    ui.radio_value(&mut self.selected.terrain, None, "None");
                });

                ui.horizontal(|ui| {
                    for animal in [Animal::Bear, Animal::Cougar] {
                        ui.radio_value(&mut self.selected.animal, Some(animal), animal.to_string());
                    }
                    ui.radio_value(&mut self.selected.animal, None, "None");
                });

                ui.horizontal(|ui| {
                    for color in [
                        BuildingColor::Green,
                        BuildingColor::Blue,
                        BuildingColor::White,
                    ] {
                        for ty in [BuildingType::Shack, BuildingType::Stone] {
                            let building = Building { color, ty };
                            ui.radio_value(
                                &mut self.selected.building,
                                Some(building),
                                building.to_string(),
                            );
                        }
                    }
                    ui.radio_value(&mut self.selected.building, None, "None");
                });
            });
            ui.separator();

            ui.horizontal(|ui| {
                for player in 0..self.board.players() {
                    ui.radio_value(
                        &mut self.selected.player,
                        Some(player),
                        format!("Player {}", player + 1),
                    );
                }
                ui.radio_value(&mut self.selected.player, None, "None");
            });

            // TODO Nicer way to communicate this state
            let mut changed = false;
            ui.add(Map {
                board: &mut self.board,
                stats: self.stats.as_ref(),
                selected: &self.selected,
                changed_ret: &mut changed,
            });
            if changed {
                self.recalculate();
            }
        });
    }
}

struct Map<'a> {
    board: &'a mut Board,
    stats: Option<&'a Stats>,
    selected: &'a Selected,
    changed_ret: &'a mut bool,
}

impl Widget for Map<'_> {
    fn ui(self, ui: &mut egui::Ui) -> egui::Response {
        let size = ui.available_size();
        let (resp, mut painter) = ui.allocate_painter(size, Sense::click_and_drag());

        #[derive(Debug)]
        struct TileInfo<'a> {
            x: f32,
            y: f32,
            hex: Hex,
            tile: &'a mut Tile,
        }

        impl<'a> TileInfo<'a> {
            fn new(hex: Hex, tile: &'a mut Tile) -> Self {
                let x = hex.y() as f32 * 1.5;
                let y = hex.x() as f32 * 3.0f32.sqrt() + hex.y() as f32 * 3.0f32.sqrt() / 2.0;
                TileInfo { x, y, hex, tile }
            }
        }

        let mut tile_infos: Vec<TileInfo> = self
            .board
            .iter_mut()
            .map(|(hex, tile)| TileInfo::new(hex, tile))
            .collect();

        let max_x = tile_infos
            .iter()
            .map(|tile| tile.x)
            .max_by(|a, b| a.partial_cmp(b).expect("x is nan"))
            .expect("no tiles");
        let max_y = tile_infos
            .iter()
            .map(|tile| tile.y)
            .max_by(|a, b| a.partial_cmp(b).expect("y is nan"))
            .expect("no tiles");
        let scale = resp.rect.size() / Vec2::new(max_x + 2.0, max_y + 2.0);

        let mut interacted_tile = None;
        let mut hovered_tile = None;

        for info in tile_infos.iter_mut() {
            let center = resp.rect.min + scale * Vec2::new(info.x + 1.0, info.y + 1.0);

            painter.add(hex_shape(
                center,
                scale - Vec2::new(2.0, 2.0),
                Stroke::new(3.0, info.tile.terrain.color()),
                Color32::TRANSPARENT,
            ));

            if let Some(building) = info.tile.building {
                painter.add(building_shape(
                    building,
                    center,
                    scale * 0.3,
                    Stroke::none(),
                    building.color.color(),
                ));
            }

            if let Some(animal) = info.tile.animal {
                painter.add(hex_shape(
                    center,
                    scale * 0.8,
                    Stroke::new(2.5, animal.color()),
                    Color32::TRANSPARENT,
                ));
            }

            for (player, &val) in info.tile.player_info.iter().enumerate() {
                if let Some(val) = val {
                    painter.add(info_shape(
                        val,
                        center
                            + scale
                                * 0.4
                                * Vec2::angled(
                                    2.0 * PI * player as f32 / info.tile.player_info.len() as f32,
                                ),
                        scale * 0.15,
                        Stroke::none(),
                        Player(player).color(),
                    ));
                }
            }

            if let Some(stats) = self.stats {
                if stats.solution_for(info.hex).is_some() {
                    let galley = painter.layout_no_wrap(
                        "?".to_owned(),
                        FontId::proportional(24.0),
                        Color32::BLACK,
                    );
                    painter.galley(center - galley.mesh_bounds.size() * 0.5, galley);
                }
            }

            if let Some(pos) = resp.hover_pos() {
                if ((center.to_vec2() - pos.to_vec2()) / scale).length() < 3.0f32.sqrt() / 2.0 {
                    hovered_tile = Some((info.hex, pos));
                }
            }

            if let Some(pos) = resp.interact_pointer_pos() {
                if ((center.to_vec2() - pos.to_vec2()) / scale).length() < 3.0f32.sqrt() / 2.0 {
                    interacted_tile = Some(info);
                }
            }
        }

        // Make sure to draw overlay after all tiles and ignore clip.
        if let Some(info) = interacted_tile {
            if resp.dragged() {
                fn assign<T>(selection: Option<T>, target: &mut Option<T>, clear: bool) {
                    if let Some(value) = selection {
                        if clear {
                            *target = None;
                        } else if target.is_none() {
                            *target = Some(value);
                        }
                    }
                }

                let clear = ui.input().pointer.button_down(PointerButton::Secondary);
                assign(self.selected.terrain, &mut info.tile.terrain, clear);
                assign(self.selected.animal, &mut info.tile.animal, clear);
                assign(self.selected.building, &mut info.tile.building, clear);
                // TODO Potentially register change from here but need to be careful about slowdown.
            }

            if resp.clicked() {
                if let Some(player) = self.selected.player {
                    info.tile.player_info[player] = match info.tile.player_info[player] {
                        None => Some(true),
                        Some(true) => Some(false),
                        Some(false) => None,
                    };
                    *self.changed_ret = true;
                }
            }
            if resp.secondary_clicked() {
                if let Some(player) = self.selected.player {
                    info.tile.player_info[player] = match info.tile.player_info[player] {
                        None => Some(false),
                        Some(false) => Some(true),
                        Some(true) => None,
                    };
                    *self.changed_ret = true;
                }
            }
        }

        painter.set_clip_rect(Rect::EVERYTHING);
        if let Some((hex, pos)) = hovered_tile {
            if let Some(stats) = self.stats {
                let format = TextFormat {
                    color: Color32::BLACK,
                    ..Default::default()
                };

                let mut job = LayoutJob {
                    halign: Align::Min,
                    justify: false,
                    ..Default::default()
                };

                job.append(
                    &if let Some(ruleset) = stats.solution_for(hex) {
                        Iterator::chain(
                            iter::once(String::from("POSSIBLE\n")),
                            ruleset.iter().map(|rule| format!("- {}\n", rule)),
                        )
                        .collect::<String>()
                    } else {
                        String::from("NO\n")
                    },
                    0.0,
                    format.clone(),
                );

                job.append(
                    &stats
                        .matching_rules_for(hex)
                        .iter()
                        .map(|rule| format!("{}\n", rule))
                        .collect::<String>(),
                    0.0,
                    format,
                );

                let galley = painter.ctx().fonts().layout_job(job);
                let mesh_size = galley.mesh_bounds;
                let draw_pos = pos - Vec2::new(0.0, mesh_size.height());
                painter.rect_filled(
                    mesh_size.translate(draw_pos.to_vec2()),
                    Rounding::none(),
                    Color32::LIGHT_GRAY,
                );
                painter.galley(draw_pos, galley);
            }
        }

        resp
    }
}

fn polygon_shape(
    n: u32,
    angle_offset: f32,
    center: Pos2,
    scale: Vec2,
    stroke: Stroke,
    fill: Color32,
) -> Shape {
    Shape::convex_polygon(
        (0..n)
            .map(|i| center + Vec2::angled(angle_offset + 2.0 * PI / n as f32 * i as f32) * scale)
            .collect(),
        fill,
        stroke,
    )
}

fn hex_shape(center: Pos2, scale: Vec2, stroke: Stroke, fill: Color32) -> Shape {
    polygon_shape(6, 0.0, center, scale, stroke, fill)
}

fn building_shape(
    building: Building,
    center: Pos2,
    scale: Vec2,
    stroke: Stroke,
    fill: Color32,
) -> Shape {
    match building.ty {
        BuildingType::Shack => polygon_shape(3, PI / 6.0, center, scale, stroke, fill),
        BuildingType::Stone => polygon_shape(8, 0.0, center, scale, stroke, fill),
    }
}

fn info_shape(info: bool, center: Pos2, scale: Vec2, stroke: Stroke, fill: Color32) -> Shape {
    match info {
        false => polygon_shape(4, PI / 4.0, center, scale, stroke, fill),
        true => polygon_shape(24, 0.0, center, scale, stroke, fill),
    }
}

trait Colored {
    fn color(&self) -> Color32;
}

impl Colored for Option<Terrain> {
    fn color(&self) -> Color32 {
        match self {
            None => Color32::from_rgb(200, 200, 200),
            Some(terrain) => match terrain {
                Terrain::Forest => Color32::from_rgb(22, 226, 58),
                Terrain::Swamp => Color32::from_rgb(171, 59, 242),
                Terrain::Mountain => Color32::from_rgb(91, 91, 86),
                Terrain::Desert => Color32::from_rgb(248, 236, 84),
                Terrain::Water => Color32::from_rgb(55, 108, 244),
            },
        }
    }
}

impl Colored for BuildingColor {
    fn color(&self) -> Color32 {
        match self {
            BuildingColor::White => Color32::from_rgb(219, 212, 212),
            BuildingColor::Blue => Color32::from_rgb(122, 151, 246),
            BuildingColor::Green => Color32::from_rgb(131, 235, 99),
        }
    }
}

impl Colored for Animal {
    fn color(&self) -> Color32 {
        match self {
            Animal::Cougar => Color32::from_rgb(238, 84, 130),
            Animal::Bear => Color32::from_rgb(175, 93, 52),
        }
    }
}

#[derive(Debug, Clone, Copy)]
struct Player(usize);

impl Colored for Player {
    fn color(&self) -> Color32 {
        match self.0 {
            0 => Color32::from_rgb(210, 39, 36),
            1 => Color32::from_rgb(10, 129, 49),
            2 => Color32::from_rgb(68, 71, 169),
            3 => Color32::from_rgb(136, 81, 25),
            4 => Color32::from_rgb(160, 25, 156),
            _ => panic!("invalid player number"),
        }
    }
}
